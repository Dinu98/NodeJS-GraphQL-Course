const nodefetch = require('node-fetch');

const message = url => {
    return nodefetch(url)
    .then(res => res.json())
}

module.exports = message;