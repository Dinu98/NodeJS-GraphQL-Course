const express = require('express');
const app = express();
const port = 3000;
const message = require('./modulefile');

app.get('/hello-world',function (req,res){
    message('https://cat-fact.herokuapp.com/facts/random')
    .then( (body) => {
            const {text} = body;
            res.send(text)
    })
});

app.listen(port,function(){
    console.log('server started');
});