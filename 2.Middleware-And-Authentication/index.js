const express = require('express');
const app = express();
const port = 3000;
const message = require('./modulefile');
var jwt = require('jsonwebtoken');

const key = "Key";

app.use(express.json());

app.get('/hello-world',function (req,res){
    message('https://cat-fact.herokuapp.com/facts/random')
    .then( (body) => {
            const {text} = body;
            res.send(text)
    })
});

const authenticationMiddlerware = (req,res,next) => {
    const { authorization  } = req.headers;

    if(!authorization ){
        res.status(401).send({
            status: "you naughty naughty"
        });
    }

    const jwtToken = authorization.replace('Bearer ', '');

    jwt.verify(jwtToken,key, (err,decoded) =>{
        if(err){
            return res.status(401).send({
                status:"Nu te cunosc stimabile"
            });
        } else{
            return next();
        }

    });
};
app.post("/graphql", authenticationMiddlerware, (req,res) => {
    res.send({
        status:"Domnul a fost primit"
    });
});

app.post("/graphql/public", (req,res) => {
    const { user, pass} = req.body;

    if(user === "Stimabilul domn" && pass === "Domnul stimabil"){
        const token = jwt.sign({},key, (err,token) => {
            res.send({
                token
            });
        });
    } else {
        res.send({
            status: "not good stimabilule"
        })
    }

});

app.listen(port,function(){
    console.log('server started');
});